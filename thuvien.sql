-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2021 at 10:01 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thuvien`
--

-- --------------------------------------------------------

--
-- Table structure for table `danhmuc`
--

CREATE TABLE `danhmuc` (
  `id_Danhmuc` int(50) NOT NULL,
  `tendanhmuc` varchar(50) NOT NULL,
  `slugdanhmuc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `danhmuc`
--

INSERT INTO `danhmuc` (`id_Danhmuc`, `tendanhmuc`, `slugdanhmuc`) VALUES
(9, 'Sách khoa học', 'sach-khoa-hoc'),
(10, 'Sách lập trình', 'sach-lap-trinh'),
(12, 'Sách tham khảo', 'sach-tham-khao'),
(14, 'Sách tiếng anh', 'sach-tieng-anh');

-- --------------------------------------------------------

--
-- Table structure for table `ddc`
--

CREATE TABLE `ddc` (
  `malopDDC` varchar(50) NOT NULL,
  `tenlopDDC` varchar(50) NOT NULL,
  `slugDDC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ddc`
--

INSERT INTO `ddc` (`malopDDC`, `tenlopDDC`, `slugDDC`) VALUES
('000', 'Tổng quát', 'tong-quat'),
('100', 'Triết học', 'triet-hoc');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `muontra`
--

CREATE TABLE `muontra` (
  `id_Muontra` int(11) NOT NULL,
  `id_Sach` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `ngay_Muon` date NOT NULL,
  `ngay_Hentra` date NOT NULL,
  `ngay_Tra` date DEFAULT NULL,
  `tinhtrang` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `muontra`
--

INSERT INTO `muontra` (`id_Muontra`, `id_Sach`, `id`, `ngay_Muon`, `ngay_Hentra`, `ngay_Tra`, `tinhtrang`) VALUES
(76, 17, 3, '2021-08-07', '2022-08-07', '2021-08-07', 'Quá hạn'),
(77, 18, 3, '2021-08-07', '2022-08-07', NULL, 'Đang mượn'),
(78, 17, 5, '2021-08-07', '2022-08-07', NULL, 'Đang mượn'),
(79, 15, 5, '2021-08-07', '2022-08-07', '2021-08-07', 'Đã trả'),
(80, 14, 5, '2021-08-07', '2022-08-07', '2021-08-07', 'Đã trả');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('tranmanhduc0964875742@gmail.com', '$2y$10$WuAz6eib6y8Skoe4AMvNHOZ4UiNnG9tuSMfxYEKmQ4JIhHr.n91Rm', '2021-07-31 19:23:36'),
('ductm00@gmail.com', '$2y$10$qbhqhmpeStgZ.w4sngLaMOtZ9CwStE/qaeUIiUS8.HcHbohp.wnP2', '2021-07-31 19:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `sach`
--

CREATE TABLE `sach` (
  `id_Sach` int(50) NOT NULL,
  `tensach` varchar(50) NOT NULL,
  `slugsach` varchar(100) NOT NULL,
  `tentacgia` varchar(50) NOT NULL,
  `id_Danhmuc` varchar(50) NOT NULL,
  `malopDDC` int(10) NOT NULL,
  `nhaxuatban` varchar(50) NOT NULL,
  `namxuatban` int(10) NOT NULL,
  `hinhanh` varchar(100) NOT NULL,
  `soluong` int(50) DEFAULT NULL,
  `giabia` int(50) NOT NULL,
  `tomtat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sach`
--

INSERT INTO `sach` (`id_Sach`, `tensach`, `slugsach`, `tentacgia`, `id_Danhmuc`, `malopDDC`, `nhaxuatban`, `namxuatban`, `hinhanh`, `soluong`, `giabia`, `tomtat`) VALUES
(9, 'Code Dạo Kí Sự', 'code-dao-ki-su', 'Phạm Huy Hoàng', '10', 100, 'Nhà Xuất Bản Thanh Niên', 2018, 'codedaokysu84.jpg', 9, 127000, 'Code Dạo Kí Sự - Lập Trình Viên Đâu Phải Chỉ Biết Code\\r\\n\\r\\nNếu các bạn có đọc các blog về lập trình ở Việt Nam thì có lẽ cái tên Tôi đi code dạo không có gì quá xa lạ đối với các bạn.'),
(10, 'Giáo Trình Lập Trình C Căn Bản Và Nâng Cao', 'giao-trinh-lap-trinh-c-can-ban-va-nang-cao', 'Nhiều Tác Giả', '10', 100, 'Nhà Xuất Bản Bách Khoa Hà Nội', 2018, 'kythuatlaptrinhc16.jpg', 9, 135000, 'Giáo trình kỹ thuật lập trình C căn bản và nâng cao được hình thành qua nhiều năm giảng dạy của các tác giả. Ngôn ngữ lập trình C là một môn học cơ sở trong chương trình đào tạo kỹ sư, cử nhân tin học của nhiều trường đại học. Ở đây sinh viên được trang bị những kiến thức cơ bản nhất về lập trình, các kỹ thuật  tổ chức dữ liệu và lập trình căn bản với ngôn ngữ C.'),
(11, 'Lập Trình Java Căn Bản', 'lap-trinh-java-can-ban', 'Nhiều Tác Giả', '10', 100, 'Nhà Xuất Bản Xây Dựng', 2018, 'laptrinhjava25.jpg', 4, 85000, 'Cung cấp các kiến thức cơ bản về ngôn ngữ lập trình Java, kiến thức về lập trình hướng đối tượng, xử lý biệt lệ, lập trình đa luồng, lập trình from với swing và kết nối cơ sở dữ liệu với Java…'),
(12, 'Học Tốt Ngữ Văn Lớp 12 Toàn Tập', 'hoc-tot-ngu-van-lop-12-toan-tap', 'Lê Thi Mỹ Trinh', '12', 100, 'Nhà Xuất Bản Tp. Hồ Chí Minh', 2012, 'hoctotnguvan1262.jpg', 2, 65000, 'Chúng tôi đã bám sát nội dung của sách giáo khoa để trả lời hệ thống các câu hỏi, hướng dẫn làm bài tập; đồng thời đưa vào một số bài văn tham khảo, những đề văn mở nhằm giúp các em có định hướng cụ thể, tạo điều kiện để các em có thể học tốt môn Ngữ văn một cách chủ động, sáng tạo.'),
(14, 'Sổ tay Kiến Thức Ngữ Văn', 'so-tay-kien-thuc-ngu-van', 'Lê Nguyên Lâm', '12', 100, 'Nhà Xuất Bản Đại Học Quốc Gia Hà Nội', 2014, 'sotaynguvan11.jpg', 3, 60000, 'Cuốn Sổ tay kiến thức Ngữ văn Trung học phổ thông được biên soạn nhằm hệ thống hóa toàn bộ kiến thức Ngữ văn trong chương trình Trung học phổ thông – giai đoạn quan trọng để chuẩn bị cho kì thi tốt nghiệp phổ thông và đại học.'),
(15, 'Tài Liệu Luyện Thi Tổng Hợp VSTEP Bậc 3', 'tai-lieu-luyen-thi-tong-hop-vstep-bac-3', 'Nhiều Tác Giả', '14', 100, 'Nhà Xuất Bản Tp. Hồ Chí Minh', 2017, 'vstepbac333.jpg', 3, 280000, 'Nhanh chóng nắm vững kiến thức cần thiết cũng như kỹ năng làm bài thi B1 trong thời gian ngắn'),
(16, 'Hackers Toeic Vocabulary', 'hackers-toeic-vocabulary', 'David Cho', '14', 100, 'Nhà Xuất Bản Dân Trí', 2021, 'toeic75.jpg', 9, 249000, 'Hackers TOEIC Vocabulary là cuốn sách được thiết kế nhằm giúp người học có thể hoàn thành mục tiêu nhớ và nắm được cách sử dụng của khoảng gần 7600 từ vựng theo các chủ đề khác nhau trong vòng 30 ngày. Ngoài ra, bạn còn được cung cấp những nội dung cần thiết liên quan như: câu hỏi theo định dạng bài thi thật, các xu hướng ra đề của bài thi dạng thức mới.'),
(17, 'Mastering Cambridge Ielts Practice Tests 1', 'mastering-cambridge-ielts-practice-tests-1', 'Xuân Lan', '14', 100, 'Nhà Xuất Bản Đà Nẵng', 2001, 'ielts133.jpg', 12, 300000, 'Cẩm Nang Luyện Thi Ielts - Mastering Cambridge Ielts Practice Tests 1 With Answers'),
(18, 'Bách Khoa Thư Về Khoa Học', 'bach-khoa-thu-ve-khoa-hoc', 'Nhiều Tác Giả', '9', 100, 'Nhà Xuất Bản Thanh Niên', 2018, 'bachkhoatoanthuvekhoahoc6.jpg', 9, 320000, 'BÁCH KHOA THƯ VỀ KHOA HỌC tóm lược kiến thức khoa học của loài người suốt nhiều thế kỷ trong một cuốn sách. Những hiện tượng tự nhiên, những phát minh có tính đột phá, những sự thật khoa học và những vấn đề mới nhất được giải thích thật đơn giản'),
(19, 'Bách Khoa Lịch Sử Thế Giới', 'bach-khoa-lich-su-the-gioi', 'The Usborne', '9', 100, 'Nhà Xuất Bản Dân Trí', 2015, 'licsuthegioi36.jpg', 12, 250000, 'Bách khoa Lịch sử thế giới là ấn phẩm vừa được phát hành thuộc tủ sách Bách khoa tri thức của Công ty Văn hóa Đông A. Cuốn sách chứa đựng lượng kiến thức khổng lồ về thế giới từ trước đến nay với hơn 200 chủ đề nằm trong 4 phần chính: Thế giới Tiền sử, Thế giới Cổ đại, Thế giới Trung cổ và 500 năm gần đây.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `address`) VALUES
(1, 'Đức', 'tranmanhduc0964875742@gmail.com', NULL, '$2y$10$tdz1mqzztnWRQcX8D2/2a.xsalyVCcpkZd/Pyb8xDqNNS1huya0uC', 'UyVWdMYgrSQXiASlBtXx8828GB32KT2D2vg37N9x0pDxNgy97Yaoacmq4ckK', '2021-07-14 03:55:51', '2021-07-14 03:55:51', '', ''),
(2, 'Đứcc', 'ductm00@gmail.com', NULL, '$2y$10$Em4irEIqP9nb98KtS7mjFuTefmtwqa9blLHI/qQrWsZxA.6d5mRAO', NULL, '2021-07-28 00:53:29', '2021-07-28 00:53:29', '', ''),
(3, 'Đứccc', '18020341@vun.edu.vn', NULL, '$2y$10$c05djjCsjpTGaq4MIJWVhOAwXRQX9E5C1ZvkFrZUz1cFwWXlsDUo2', '9MSHRXo7TZAmvvZ0hDlAtqHQ9ZGR1x4IUtkoYHXpqNBXVMoiiyb3VGzcJF2g', '2021-07-28 01:44:06', '2021-07-28 01:44:06', '', ''),
(4, 'Đức', 'fffff@gmail.com', NULL, '$2y$10$x3vFxcubdSTFf2HZzIMb2.61b08NnAw3Zhcfqkbc1Q.ob.upXoUtG', NULL, '2021-07-31 02:18:57', '2021-07-31 02:18:57', '0964875742', 'trung hoà cầu giấy'),
(5, 'Nguyễn Hồng Thái', 'thai@gmail.com', NULL, '$2y$10$qMv/Fga4/RciAVW88KgRueH2ycA7nXS9lCJMQ.tYAT5DSoej4tX3K', NULL, '2021-08-02 01:33:37', '2021-08-02 01:33:37', '0327635814', 'trung hoà cầu giấy'),
(6, 'Nguyễn Hồng Thái', 'thaidui@gmail.com', NULL, '$2y$10$X6Es.OpcDBRpN6mChDc6mOI9Xy1mjwe7naEPttHXlY.3Tzyh9rGTa', NULL, '2021-08-04 18:12:20', '2021-08-04 18:12:20', '0931799675', '196 cầu giấy'),
(7, 'admin', 'admin@gmail.com', NULL, '$2y$10$Dzjy8LtTchulcGZ2uxW2pOlH3scHm1ABRDnfo90yCYKA22bQnQRl2', 'WfdwPX8NDLhOs4MrppQl4zWN0fdKoFrNZrxWENP1fTmKjL9vIfbAeBk9pJNG', '2021-08-06 00:41:02', '2021-08-06 00:41:02', '0931799674', 'trung hoà cầu giấy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `danhmuc`
--
ALTER TABLE `danhmuc`
  ADD PRIMARY KEY (`id_Danhmuc`);

--
-- Indexes for table `ddc`
--
ALTER TABLE `ddc`
  ADD PRIMARY KEY (`malopDDC`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `muontra`
--
ALTER TABLE `muontra`
  ADD PRIMARY KEY (`id_Muontra`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sach`
--
ALTER TABLE `sach`
  ADD PRIMARY KEY (`id_Sach`),
  ADD KEY `malopDDC` (`malopDDC`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `danhmuc`
--
ALTER TABLE `danhmuc`
  MODIFY `id_Danhmuc` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `muontra`
--
ALTER TABLE `muontra`
  MODIFY `id_Muontra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `sach`
--
ALTER TABLE `sach`
  MODIFY `id_Sach` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
